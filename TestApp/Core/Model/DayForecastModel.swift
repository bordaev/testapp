//
//  DayForecastModel.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/2/20.
//

import Foundation

public class ForecastModel: NSObject{
    public let time: Date
    public let mainTextInfo: String
    public let temperature: Float
    
    public let humidity: Float
    public let precipitation:Float
    public let pressure: Float
    public let windSpeed: Float
    public let windDeg: Float
    
    public let city: String
    public let countryCode: String
    public let icon: String
    
    init(current: CurrentWeatherResp, date: Date) {
        if let gen = current.weather.first {
            self.mainTextInfo = gen.main
            self.icon = gen.icon
        }else{
            self.mainTextInfo = ""
            self.icon = ""
        }
        
        self.temperature = ForecastModel.convertToC(f: current.main.temp)
        
        self.humidity = current.main.humidity
        self.pressure = current.main.pressure
        self.windSpeed = current.wind.speed
        self.windDeg = current.wind.deg
        
        self.city = current.name
        self.countryCode = current.sys.country
        
        self.time = date
        
        if let volume = current.rain?.h3 {
            self.precipitation = volume
        } else if let volume = current.snow?.h3{
            self.precipitation = volume
        }else{
            self.precipitation = 0
        }
        
        super.init()
    }
    
    init(weatherItem: ItemWeather5DayResp, city: String, country: String) {
        if let gen = weatherItem.weather.first {
            self.mainTextInfo = gen.main
            self.icon = gen.icon
        }else{
            self.mainTextInfo = ""
            self.icon = ""
        }
        
        self.temperature = ForecastModel.convertToC(f: weatherItem.main.temp)
        
        self.humidity = weatherItem.main.humidity
        self.pressure = weatherItem.main.pressure
        self.windSpeed = weatherItem.wind.speed
        self.windDeg = weatherItem.wind.deg
        
        self.city = city
        self.countryCode = country
        
        self.time = Date.init(timeIntervalSince1970: TimeInterval(weatherItem.dt))
        
        if let volume = weatherItem.rain?.h3 {
            self.precipitation = volume
        } else if let volume = weatherItem.snow?.h3{
            self.precipitation = volume
        }else {
            self.precipitation = 0
        }
        
        super.init()
    }
    
    static func convertToC(f: Float)->Float{
        return (f-273.15)
    }
}
