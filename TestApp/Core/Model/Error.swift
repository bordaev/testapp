//
//  Error.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/2/20.
//

import Foundation


struct MyError {
    public let descriprion: String
    public let code: Int
    
    init(code: Int = 0, description: String?) {
        self.code = 0
        if let des = description {
            self.descriprion = des
        }else{
            self.descriprion = NSLocalizedString("main.error.escription", comment: "")
        }
    }
}
