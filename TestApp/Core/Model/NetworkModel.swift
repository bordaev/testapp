//
//  File.swift
//  TestApp
//
//  Created by Alex Bordaev on 1/31/20.
//

import Foundation

public struct CurrentWeatherResp: Codable{
    public let coord: Coordinate
    public let weather: [WeatherGen]
    public let base: String
    public let main: CurrentWeatherMainResp
    public let wind: WindResp
    public let clouds: CloudsResp
    public let rain: Volume?
    public let snow: Volume?
    public let dt: Int
    public let sys: SysRespDay
    public let timezone: Int
    public let id: Int
    public let name: String
    public let cod: Int
}

public struct Forecast5dayResp: Codable{
    public let cod: String
    public let message: Float
    public let cnt: Int
    public let list: [ItemWeather5DayResp]
    public let city: City5DayResp
}

public struct Coordinate: Codable{
    public let lon: Float
    public let lat: Float
}

public struct WeatherGen: Codable{
    public let id: Int
    public let main: String
    public let description: String
    public let icon: String
}

public struct CurrentWeatherMainResp: Codable{
    public let temp: Float
    public let feels_like: Float
    public let temp_min: Float
    public let temp_max: Float
    public let pressure: Float
    public let humidity: Float
}

public struct WindResp: Codable{
    public let speed: Float
    public let deg: Float
}

public struct SysRespDay: Codable{
    public let type: Int
    public let id: Int
//    public let message: Float
    public let country: String
    public let sunrise: Int64
    public let sunset: Int64
}

public struct CloudsResp: Codable{
    public let all: Int
}

public struct Sys5DayResp: Codable{
    public let pod: String
}

public struct City5DayResp: Codable{
    public let id: Int
    public let name: String
    public let coord: Coordinate
    public let country: String
    public let timezone: Int
    public let sunset: Int
    public let sunrise: Int
}

public struct MainWeather5DayResp: Codable{
    public let temp: Float
    public let feels_like: Float
    public let temp_min: Float
    public let temp_max: Float
    public let pressure: Float
    public let sea_level: Float
    public let grnd_level: Float
    public let humidity: Float
    public let temp_kf: Float
}

public struct ItemWeather5DayResp:Codable{
    public let dt: Double
    public let main: MainWeather5DayResp
    public let weather: [WeatherGen]
    public let clouds: CloudsResp
    public let wind: WindResp
    public let sys: Sys5DayResp?
    public let rain: Volume?
    public let snow: Volume?
    public let dt_txt: String
    
}

public struct ErroResp:Codable{
    public let cod: Int
    public let message: String
}

public struct Volume: Codable{
    public let h3: Float?
    public let h1: Float?
    
    private enum CodingKeys: String, CodingKey {
        case h3 = "3h"
        case h1 = "1h"
    }
}


//MARK:-- responses examples

//MARK: response Current weather
//{"coord": { "lon": 139,"lat": 35},
//  "weather": [
//    {
//      "id": 800,
//      "main": "Clear",
//      "description": "clear sky",
//      "icon": "01n"
//    }
//  ],
//  "base": "stations",
//  "main": {
//    "temp": 281.52,
//    "feels_like": 278.99,
//    "temp_min": 280.15,
//    "temp_max": 283.71,
//    "pressure": 1016,
//    "humidity": 93
//  },
//  "wind": {
//    "speed": 0.47,
//    "deg": 107.538
//  },
//  "clouds": {
//    "all": 2
//  },
//  "dt": 1560350192,
//  "sys": {
//    "type": 3,
//    "id": 2019346,
//    "message": 0.0065,
//    "country": "JP",
//    "sunrise": 1560281377,
//    "sunset": 1560333478
//  },
//  "timezone": 32400,
//  "id": 1851632,
//  "name": "Shuzenji",
//  "cod": 200
//}

//MARK: response 5 day weather
//{
//"cod": "200",
//"message": 0,
//"cnt": 40,
//  "list": [
//    {
//      "main": {
//        "temp": 284.92,
//        "feels_like": 281.38,
//        "temp_min": 283.58,
//        "temp_max": 284.92,
//        "pressure": 1020,
//        "sea_level": 1020,
//        "grnd_level": 1016,
//        "humidity": 90,
//        "temp_kf": 1.34
//      },
//      "weather": [
//        {
//          "id": 804,
//          "main": "Clouds",
//          "description": "overcast clouds",
//          "icon": "04d"
//        }
//      ],
//      "clouds": {
//        "all": 100
//      },
//      "wind": {
//        "speed": 5.19,
//        "deg": 211
//      },
//      "sys": {
//        "pod": "d"
//      },
//      "dt_txt": "2020-01-07 15:00:00",
//       "dt": 1578409200
//    },
//
//    ...
//
//"city": {
//    "id": 2643743,
//    "name": "London",
//    "coord": {
//      "lat": 51.5073,
//      "lon": -0.1277
//    },
//    "country": "GB",
//    "timezone": 0,
//    "sunrise": 1578384285,
//    "sunset": 1578413272
//  }
//}
