//
//  ForecastService.swift
//  TestApp
//
//  Created by Alex Bordaev on 1/31/20.
//

import Foundation

protocol ForecastService: AnyObject {
    func curentWeather(lat:Double, lon: Double, force: Bool, complection: @escaping(MyError?, ForecastModel? ) ->())
    func forecast5Day(lat:Double, lon: Double, force: Bool, complection: @escaping(MyError?, [ForecastModel]? ) ->())
}

class ForecastServiceImpl: NSObject{
    private let appId: String = "59bcfa14729bf995c621d06e206a4d7a"
    private var cashCurrentWeathr: WeatherCash? = nil
    private var cash5Day: WeatherCash? = nil
    
}

extension ForecastServiceImpl: ForecastService{
    func curentWeather(lat:Double, lon: Double, force: Bool, complection: @escaping(MyError?, ForecastModel? ) ->()){
        var forecast: ForecastModel? = nil;
        
        if let cashCurrent = cashCurrentWeathr{
            if  Date().timeIntervalSince(cashCurrent.reqTime) < 60*10 && force == false {
                forecast = cashCurrent.days.first
            }else{
                self.cashCurrentWeathr = nil
            }
        }
        
        if let forecast = forecast {
            DispatchQueue.main.async{
                complection(nil,forecast)
            }
        }else{
            //(lon: 27.566668, lat: 53.9000)
            self.requestCurrentWeather(lon: lon, lat: lat) { (error, model) in
                if let model = model {
                    DispatchQueue.main.async{
                        self.cashCurrentWeathr = WeatherCash(reqTime: Date(), days: [model])
                    }
                }
                DispatchQueue.main.async{
                    complection(error,model)
                }
            }
        }
    }
    
    func forecast5Day(lat:Double, lon: Double, force: Bool, complection: @escaping(MyError?, [ForecastModel]? ) ->()){
        var forecast: [ForecastModel]? = nil;
        
        if let cash = cash5Day{
            if  Date().timeIntervalSince(cash.reqTime) < 60*10 && force == false {
                forecast = cash.days
            }else{
                self.cash5Day = nil
            }
        }
        
        if let forecast = forecast {
            DispatchQueue.main.async{
                complection(nil,forecast)
            }
        }else{
            self.request5dayWeather(lon: lon, lat: lat) { (error, list) in
                if let list = list {
                    DispatchQueue.main.async {
                        self.cash5Day = WeatherCash(reqTime: Date(), days: list)
                    }
                }
                DispatchQueue.main.async{
                    complection(error,list)
                }
            }
        }
    }
}

// MARK: network requests

private extension ForecastServiceImpl{
    func requestCurrentWeather(lon: Double, lat: Double, complection: @escaping(MyError?, ForecastModel? ) ->()) {

        let strUrl = "https://api.openweathermap.org/data/2.5/weather?lat=\(lat)&lon=\(lon)&APPID=59bcfa14729bf995c621d06e206a4d7a"
        
        let url = URL(string: strUrl )!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                let err = MyError.init(code: -1, description: error.localizedDescription)
                complection(err,nil)
            } else {
                if let err = self.checkRespStatusErors(response: response){
                    complection(err,nil)
                }else{
                    if let data = data{
                        do {
                            let resp = try JSONDecoder().decode(CurrentWeatherResp.self, from: data)
                            let forecast = ForecastModel.init(current: resp, date: Date())
                            complection(nil,forecast)
                        } catch _ {
                            do {
                                let resp = try JSONDecoder().decode(ErroResp.self, from: data)
                                let err = MyError.init(code: resp.cod, description: resp.message)
                                complection(err,nil)
                            } catch _ {
                                let err = MyError.init(code: -1, description: nil)
                                complection(err,nil)
                            }
                        }
                    }else{
                        let err = MyError.init(code: -1, description: nil)
                        complection(err,nil)
                    }
                }
            }
        }
        task.resume()
    }
    
    func request5dayWeather(lon: Double, lat: Double, complection: @escaping(MyError?, [ForecastModel]? ) ->()) {

        let strUrl = "https://api.openweathermap.org/data/2.5/forecast?lat=\(lat)&lon=\(lon)&APPID=c5e6dd5a8d4e64b23487195233dbf2b8"
        
        let url = URL(string: strUrl )!
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if let error = error {
                let err = MyError.init(code: -1, description: error.localizedDescription)
                complection(err,nil)
            } else {
                if let err = self.checkRespStatusErors(response: response){
                    complection(err,nil)
                }else{
                    if let data = data{
                        do {
                            let resp = try JSONDecoder().decode(Forecast5dayResp.self, from: data)
                            
                            let items = resp.list.map { (item) -> ForecastModel in
                                return ForecastModel(weatherItem: item, city: resp.city.name, country: resp.city.country)
                            }
                        
                            complection(nil,items)
                        } catch _ {
                            do {
                                let resp = try JSONDecoder().decode(ErroResp.self, from: data)
                                let err = MyError.init(code: resp.cod, description: resp.message)
                                complection(err,nil)
                            } catch _ {
                                let err = MyError.init(code: -1, description: nil)
                                complection(err,nil)
                            }
                        }
                    }else{
                        let err = MyError.init(code: -1, description: nil)
                        complection(err,nil)
                    }
                }
            }
        }
        task.resume()
    }
}

private extension ForecastServiceImpl{
    private func checkRespStatusErors (response: URLResponse?) -> MyError?{
        var err: MyError? = nil
        
        if let response = response as? HTTPURLResponse {
            switch response.statusCode {
            case 200:
                // status ok (no error)
                break
            // processing any http errors if need
            default:
                err = MyError.init(code: -1, description: nil)
            }
        }else{
            err = MyError.init(code: -1, description: nil)
        }
        
        return err
    }
}
