//
//  LocationService.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/2/20.
//

import Foundation
import CoreLocation

protocol LocationService {
    var location: CLLocation? { get }
    
    func subscribe(subscription: LocationServiceSubscription)
    func unSubscribe(subscription: LocationServiceSubscription)
}

protocol LocationServiceSubscription: AnyObject {
    func locationService(_ locationService: LocationService, newLat: Double, newLon: Double)
}

class LocationServiceImpl: NSObject {
    private let locationManager: CLLocationManager = CLLocationManager()
    private var lastLocation: CLLocation?
    private var needNotify: [LocationServiceSubscription] = []
    
    override init() {
        super.init()
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.headingFilter = kCLHeadingFilterNone
        locationManager.distanceFilter = 5000
        
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        self.lastLocation = locationManager.location
    }
}

extension LocationServiceImpl: LocationService{
    func unSubscribe(subscription: LocationServiceSubscription) {
        self.needNotify.removeAll{ $0 === subscription }
    }
    
    func subscribe(subscription: LocationServiceSubscription) {
        self.needNotify.append(subscription)
    }
    
    var location: CLLocation? {
        return lastLocation
    }
}

extension LocationServiceImpl: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.first else {
            return
        }
        
        lastLocation = location
        
        self.needNotify.forEach { (delegate) in
            delegate.locationService(self, newLat: location.coordinate.latitude, newLon: location.coordinate.longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .denied || status == .restricted {
            lastLocation = nil
        }else{
            locationManager.startUpdatingLocation()
            self.lastLocation = locationManager.location
        }
        
        print("location service changed authorization status - \(status)")
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("didFailWithError - \(error.localizedDescription)")
    }
}
