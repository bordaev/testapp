//
//  Navigator.swift
//  TestApp
//
//  Created by Alex Bordaev on 1/30/20.
//

import Foundation
import UIKit

class Navigator: NSObject {
    
    private let window: UIWindow
    private let tabBarController: UITabBarController
    private let  servise: ForecastService
    private let  locationService: LocationService
    
    init(window: UIWindow) {
        self.window = window
        self.tabBarController = UITabBarController.init()
        self.window.rootViewController = self.tabBarController
        
        self.locationService = LocationServiceImpl()
        self.servise = ForecastServiceImpl()
        super.init()
    }

    func resetTabBar() -> Void{
        let pr = TodayPresenterImpl(forecastService: self.servise, locationService: self.locationService)
        let todayVc = self.navigationVcWithController(vc: TodayViewImp.init(presenter: pr))
        todayVc.tabBarItem = UITabBarItem.init(title: NSLocalizedString("tabBar.today.title", comment: "") , image: UIImage.init(named: "today"), tag: 0)
        
        let pr2 = ForecastPresenterImpl(forecastService: self.servise, locationService: self.locationService)
        let forecastVc = self.navigationVcWithController(vc: ForecastViewImpl.init(presenter: pr2))
        forecastVc.tabBarItem = UITabBarItem.init(title:NSLocalizedString("tabBar.forecast.title", comment: "") , image: UIImage.init(named: "forecast"), tag: 0)
        
        tabBarController.viewControllers = [todayVc,
                                            forecastVc]
    }
    
    func navigationVcWithController(vc: UIViewController) -> UINavigationController {
        let navVC = UINavigationController.init(rootViewController: vc);
        return navVC
    }
}
