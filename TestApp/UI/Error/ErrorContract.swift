//
//  ErrorContract.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/3/20.
//

import Foundation

public protocol ErrorView: AnyObject{
    func ShowMessage(message: String)
    func dismiss()
}

public protocol ErrorPresenter: AnyObject{
    func attachView(view: ErrorView)
    func tryAgain()
}

public protocol ErrorPresenterDelegate: AnyObject{
    func errorPresenterSendTryAgain()
}
