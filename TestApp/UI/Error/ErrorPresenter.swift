//
//  ErrorPresenter.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/3/20.
//

import Foundation

class ErrorPresenterImpl: NSObject {
    private weak var view: ErrorView? = nil
    private weak var delegate: ErrorPresenterDelegate? = nil
    
    private let message: String
    
    init(message: String, delegate: ErrorPresenterDelegate) {
        self.delegate = delegate
        self.message = message
        super.init()
    }
}

extension ErrorPresenterImpl: ErrorPresenter{
    func attachView(view: ErrorView) {
        self.view = view
        view.ShowMessage(message: self.message)
    }
    
    func tryAgain() {
        self.delegate?.errorPresenterSendTryAgain()
        self.view?.dismiss()
    }
}
