//
//  ErrorView.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/3/20.
//

import Foundation
import UIKit

class ErrorViewImp: UIViewController {

    @IBOutlet private weak var messageLabel: UILabel!
    @IBOutlet private weak var btn: UIButton!
    
    var presenter: ErrorPresenter
    
    init(presenter: ErrorPresenter) {
        self.presenter = presenter
        super.init(nibName: "ErrorView", bundle: nil)
        self.modalPresentationStyle = .custom
    }
    
    required init?(coder: NSCoder) {
        fatalError("TodayViewImp init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.btn.setTitle(NSLocalizedString("error.view.tryAgain.btn.title", comment:" "), for: .normal)
        self.messageLabel.text = ""
        self.presenter.attachView(view: self)
    }
}

private extension ErrorViewImp{
    @IBAction func onTryAgain(_ sender: UIButton){
        self.presenter.tryAgain()
    }
}

extension ErrorViewImp: ErrorView{
    func ShowMessage(message: String) {
        self.messageLabel.text = message
    }
    
    func dismiss() {
        self.dismiss(animated: true, completion: nil)
    }
}
