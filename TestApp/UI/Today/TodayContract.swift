//
//  TodayContract.swift
//  TestApp
//
//  Created by Alex Bordaev on 1/30/20.
//

import Foundation
import UIKit

public protocol TodayView: AnyObject{
    func showWetherIcon(icon: UIImage)
    func showLocationLabel(location: String)
    func showMainInfo(mainInfo: String)
    
    func showHumidity(humidity: String)
    func showPrecipitation(precipitation: String)
    func showPressure(pressure: String)
    func showWind(wind: String)
    func showWindDirection(wind: String)
    
    func showLoading(show: Bool)
    func reset()
    func share(textToShare: [String])
    
    func presentError(presenter: ErrorPresenter)
}

public protocol TodayPresenter: AnyObject{
    func attachView(view: TodayView)
    func reload()
    func share()
}
