//
//  TodayPresenter.swift
//  TestApp
//
//  Created by Alex Bordaev on 1/30/20.
//

import Foundation
import UIKit

class TodayPresenterImpl: NSObject {
    private weak var myView: TodayView? = nil
    private let forecastService: ForecastService
    private let locationService: LocationService
    private var forecast: ForecastModel? = nil
    
    private var loading: Bool {
        didSet{
            self.myView?.showLoading(show: loading)
        }
    }
    
    init(forecastService: ForecastService, locationService: LocationService) {
        self.forecastService = forecastService
        self.locationService = locationService
        self.loading = false
        super.init()
    }
}

//MARK Functions
private extension TodayPresenterImpl{
    func updateView() {
        if let forecast = self.forecast{
            
            if let image = UIImage(named: forecast.icon){
                self.myView?.showWetherIcon(icon: image)
            }
            
            self.myView?.showMainInfo(mainInfo: String(format: "%.1f \u{2103} | \(forecast.mainTextInfo)", forecast.temperature))
            self.myView?.showLocationLabel(location: "\(forecast.city), \(forecast.countryCode)")
            self.myView?.showHumidity(humidity: "\(forecast.humidity)")
            self.myView?.showPrecipitation(precipitation: "\(forecast.precipitation)")
            self.myView?.showPressure(pressure: "\(forecast.pressure)")
            self.myView?.showWind(wind: "\(forecast.windSpeed)")
            self.myView?.showWindDirection(wind: "\(forecast.windDeg)")
            
        }else{
            self.myView?.reset()
        }
    }
    
    func reloadData(force: Bool) {
        //self.myView?.reset()
        if let location = self.locationService.location{
            self.requestNewData(lon: location.coordinate.longitude, lat: location.coordinate.latitude, force: force)
        }else{
            let errorMessage = NSLocalizedString("error.get.location", comment: "")
            self.showError(message: errorMessage)
        }
    }
    
    func requestNewData(lon: Double, lat: Double, force:Bool){
        self.loading = true
        self.forecastService.curentWeather(lat: lat, lon: lon, force: force) { (error, currentForecast) in
            self.loading = false
            if let forecast = currentForecast{
                self.forecast = forecast
                self.updateView()
            }else{
                var message = ""
                if let err = error{
                    message = err.descriprion
                }
                self.showError(message: message)
            }
        }
    }
    
    func showError(message: String)  {
        let pr = ErrorPresenterImpl(message: message, delegate: self)
        self.myView?.presentError(presenter: pr)
    }
}

//MARK: TodayPresenter
extension TodayPresenterImpl: TodayPresenter{
    func attachView(view: TodayView) {
        self.myView = view
        self.reloadData(force: false)
    }
    
    func reload() {
        self.reloadData(force: false)
    }
    
    func share() {
        var array:[String]  = []
        if let forecast = self.forecast{
            array.append("\(forecast.city), \(forecast.countryCode)")
            array.append(String(format: "%.1f \u{2103} | \(forecast.mainTextInfo)", forecast.temperature))
            array.append("humidity: \(forecast.humidity)")
            array.append("precipitation:\(forecast.precipitation)")
            array.append("pressure: \(forecast.pressure)")
            array.append("windSpeed: \(forecast.windSpeed)")
            array.append("windDeg: \(forecast.windDeg)")
        }
                             
        self.myView?.share(textToShare: array)
    }
}

//MARK: locationSubscription
// кольцевая ссылка 
//extension TodayPresenterImpl: LocationServiceSubscription{
//    func locationService(_ locationService: LocationService, newLat: Double, newLon: Double) {
//
//    }
//}

//MARK: error delegate
extension TodayPresenterImpl: ErrorPresenterDelegate{
    func errorPresenterSendTryAgain() {
        self.reloadData(force: true)
    }
}
