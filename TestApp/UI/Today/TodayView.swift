//
//  TodayView.swift
//  TestApp
//
//  Created by Alex Bordaev on 1/30/20.
//

import Foundation
import UIKit

class TodayViewImp: UIViewController {
    
    @IBOutlet private weak var mainScrollView: UIScrollView!
    @IBOutlet private weak var mainIconImageView: UIImageView!
    @IBOutlet private weak var locationLabel: UILabel!
    @IBOutlet private weak var mainInfoLabel: UILabel!
    
    @IBOutlet private weak var humidityLabel: UILabel!
    @IBOutlet private weak var precipitationLabel: UILabel!
    @IBOutlet private weak var pressureLabel: UILabel!
    @IBOutlet private weak var windLabel: UILabel!
    @IBOutlet private weak var windDirectionLabel: UILabel!
    
    @IBOutlet private weak var shareButton: UIButton!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    var presenter: TodayPresenter
    
    init(presenter: TodayPresenter) {
        self.presenter = presenter
        super.init(nibName: "TodayView", bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("TodayViewImp init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NSLocalizedString("today.view.title", comment: "")
        self.shareButton.setTitle(NSLocalizedString("btn.share.title", comment: ""), for: .normal)
        
        self.activityIndicator.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.activityIndicator.color = UIColor.white
        self.activityIndicator.layer.cornerRadius = self.activityIndicator.frame.width/2
        self.activityIndicator.hidesWhenStopped = true
        
        self.reset()
        self.presenter.attachView(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.reload()
    }
}

private extension TodayViewImp{
    @IBAction func onShare(_ sender: UIButton){
        self.presenter.share()
    }
}

extension TodayViewImp : TodayView{
    func presentError(presenter: ErrorPresenter) {
        let vc = ErrorViewImp(presenter: presenter)
        self.present(vc, animated: true, completion: nil)
    }
    
    func showTitle(title: String) {
        self.title = title
    }
    
    func showWetherIcon(icon: UIImage) {
        self.mainIconImageView.image = icon
    }
    
    func showLocationLabel(location: String) {
        self.locationLabel.text = location
    }
    
    func showMainInfo(mainInfo: String) {
        self.mainInfoLabel.text = mainInfo
    }
    
    func showHumidity(humidity: String) {
        self.humidityLabel.text = humidity
    }
    
    func showPrecipitation(precipitation: String) {
        self.precipitationLabel.text = precipitation
    }
    
    func showPressure(pressure: String) {
        self.pressureLabel.text = pressure
    }
    
    func showWind(wind: String) {
        self.windLabel.text = wind
    }
    
    func showWindDirection(wind: String) {
        self.windDirectionLabel.text = wind
    }
    
    func showLoading(show: Bool) {
        if show == true{
            self.view.isUserInteractionEnabled = false
            self.activityIndicator.startAnimating()
        }else{
            self.activityIndicator.stopAnimating()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func reset(){
        
        self.mainIconImageView.image = nil
        self.mainInfoLabel.text = ""
        self.locationLabel.text = ""
        
        self.humidityLabel.text = ""
        self.precipitationLabel.text = ""
        self.pressureLabel.text = ""
        self.windLabel.text = ""
        self.windDirectionLabel.text = ""
    }
    
    func share(textToShare: [String]) {
    
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view 

        activityViewController.excludedActivityTypes = [.postToFacebook, .airDrop ]

        self.present(activityViewController, animated: true, completion: nil)
    }
    
}
