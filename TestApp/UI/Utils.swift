//
//  Utils.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/2/20.
//

import Foundation

class Utils: NSObject {
    static func converDateToStringHeaderStule(_ date: Date) -> String{

        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        dateFormatter.doesRelativeDateFormatting = true
        
        return dateFormatter.string(from: date)
    }
    
    static func converTimeToStringHeaderStyle(_ date: Date) -> String{

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH:MM"

        return dateFormatter.string(from: date)
    }
}
