//
//  ForecastCantract.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/3/20.
//

import Foundation

public protocol ForecastView: AnyObject{
    func showTitle(title: String)
    func showItems(sections: [SectionItem])
    
    func showLoading(show: Bool)
    func reset()
    
    func presentError(presenter: ErrorPresenter)
}

public protocol ForecastPresenter: AnyObject{
    func attachView(view: ForecastView)
    func reload()
}

