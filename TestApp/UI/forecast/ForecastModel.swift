//
//  ForecastModel.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/3/20.
//

import Foundation
import UIKit

public struct CellModel{
    let icon: UIImage?
    let time: String
    let mainDescription: String
    let temperature: String
}

public struct SectionItem {
    let name: String
    let items: [CellModel]
}
