//
//  ForecastPresenter.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/3/20.
//

import Foundation
import UIKit

class ForecastPresenterImpl: NSObject {
    private weak var view: ForecastView? = nil
    private let forecastService: ForecastService
    private let locationService: LocationService
    private var forecastResult: [ForecastModel]? = nil
    
    private var loading: Bool {
        didSet{
            self.view?.showLoading(show: loading)
        }
    }
    
    init(forecastService: ForecastService, locationService: LocationService) {
        self.forecastService = forecastService
        self.locationService = locationService
        self.loading = false
        super.init()
    }
}

//MARK Functions
private extension ForecastPresenterImpl{
    func updateView() {
        if let res = forecastResult{
            var dict: [String: [CellModel]] = [:]
            var keys: [String] = []
            
            res.forEach { (item) in
                let header = Utils.converDateToStringHeaderStule(item.time)
                
                let image = UIImage(named: item.icon)
                let time = Utils.converTimeToStringHeaderStyle(item.time)
                let cellModel = CellModel(icon: image, time: time, mainDescription: item.mainTextInfo, temperature: String(format: "%.1f \u{2103}", item.temperature))
                
                if dict[header] != nil {
                    dict[header]?.append(cellModel)
                }else{
                    dict[header] = [cellModel]
                    keys.append(header)
                }
            }
            
            var sections: [SectionItem] = []
            
            keys.forEach { (key) in
                if let items = dict[key]{
                    let section = SectionItem(name: key.uppercased(), items: items)
                    sections.append(section)
                }
            }
            
            self.view?.showItems(sections: sections)
            
            if let item = res.first{
                self.view?.showTitle(title: "\(item.city)")
            }else{
                self.view?.showTitle(title: "")
            }
        }else{
            self.view?.reset()
        }
    }

    func reloadData(force: Bool) {
        self.view?.reset()
        if let location = self.locationService.location{
            self.requestNewData(lon: location.coordinate.longitude, lat: location.coordinate.latitude, force: force)
        }else{
            let errorMessage = NSLocalizedString("error.get.location", comment: "")
            self.showError(message: errorMessage)
        }
    }

    func requestNewData(lon: Double, lat: Double, force:Bool){
        self.loading = true
        self.forecastService.forecast5Day(lat: lat, lon: lon, force: force) { (error, forecast) in
            self.loading = false
            if let  forecast = forecast{
                self.forecastResult = forecast
                self.updateView()
            }else{
                var message = ""
                if let err = error{
                    message = err.descriprion
                }
                self.showError(message: message)
            }
        }
    }

    func showError(message: String)  {
        let pr = ErrorPresenterImpl(message: message, delegate: self)
        self.view?.presentError(presenter: pr)
    }
}

//MARK: ForecastPresenter
extension ForecastPresenterImpl: ForecastPresenter{
    func reload() {
        self.updateView()
    }
    
    func attachView(view: ForecastView) {
        self.view = view
        self.reloadData(force: false)
    }
}

//MARK: error delegate
extension ForecastPresenterImpl: ErrorPresenterDelegate{
    func errorPresenterSendTryAgain() {
        self.reloadData(force: true)
    }
}
