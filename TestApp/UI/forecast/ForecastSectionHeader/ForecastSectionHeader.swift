//
//  ForecastSectionHeader.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/3/20.
//

import Foundation
import UIKit

class ForecastSectionHeader: UITableViewHeaderFooterView{
    static public let sectionHeaderIdentifier: String = "ForecastSectionHeaderID"
    
    @IBOutlet private var label: UILabel!
    
    public func updateName(name: String?){
        label.text = name
    }
}
