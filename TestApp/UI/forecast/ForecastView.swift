//
//  ForecastView.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/3/20.
//

import Foundation
import UIKit

class ForecastViewImpl: UIViewController {
    @IBOutlet private weak var tableView: UITableView!
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    
    private var items: [SectionItem] = []
    private var presenter: ForecastPresenter

    init(presenter: ForecastPresenter) {
       self.presenter = presenter
       super.init(nibName: "ForecastView", bundle: nil)
    }

    required init?(coder: NSCoder) {
       fatalError("ForecastViewImpl init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = ""
        
        self.activityIndicator.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.activityIndicator.color = UIColor.white
        self.activityIndicator.layer.cornerRadius = self.activityIndicator.frame.width/2
        self.activityIndicator.hidesWhenStopped = true
        
        self.tableView.register(UINib(nibName: "ForecastCell", bundle: nil), forCellReuseIdentifier: ForecastCell.cellIdentifier)
        self.tableView.register(UINib(nibName: "ForecastSectionHeader", bundle: nil), forHeaderFooterViewReuseIdentifier: ForecastSectionHeader.sectionHeaderIdentifier)
        self.tableView.tableFooterView = UIView(frame: CGRect.zero)
        
        self.presenter.attachView(view: self)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.reload()
    }
}

extension ForecastViewImpl: ForecastView{
    func showTitle(title: String) {
        self.navigationItem.title = title
    }
    
    func showItems(sections: [SectionItem]) {
        self.items = sections
        self.tableView.reloadData()
    }
    
    func showLoading(show: Bool) {
        if show == true{
            self.view.isUserInteractionEnabled = false
            self.activityIndicator.startAnimating()
        }else{
            self.activityIndicator.stopAnimating()
            self.view.isUserInteractionEnabled = true
        }
    }
    
    func reset() {
        self.items = []
        self.tableView.reloadData()
    }
    
    func presentError(presenter: ErrorPresenter) {
        let vc = ErrorViewImp(presenter: presenter)
        self.present(vc, animated: true, completion: nil)
    }
}

extension ForecastViewImpl: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return self.items.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let section = self.items[section]
        return section.items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let section = self.items[indexPath.section]
        
        let cell = tableView.dequeueReusableCell(withIdentifier:ForecastCell.cellIdentifier) as! ForecastCell
        cell.update(cellModel: section.items[indexPath.row])
        return cell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //let sectionItem = self.items[section]
        return " "
    }

}

extension ForecastViewImpl: UITableViewDelegate{

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: ForecastSectionHeader.sectionHeaderIdentifier) as! ForecastSectionHeader
        header.updateName(name: self.items[section].name)
        return header
    }
}
