//
//  ForecastCell.swift
//  TestApp
//
//  Created by Alex Bordaev on 2/3/20.
//

import Foundation
import UIKit

class ForecastCell: UITableViewCell {
    static public let cellIdentifier: String = "ForecastCellID"
    
    @IBOutlet private weak var iconView: UIImageView!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    @IBOutlet private weak var tempLabel: UILabel!
    
    override func prepareForReuse() {
        self.iconView.image = nil
        self.timeLabel.text = ""
        self.descriptionLabel.text = ""
        self.tempLabel.text = ""
    }
    
    public func update(cellModel: CellModel){
        self.iconView.image = cellModel.icon
        self.timeLabel.text = cellModel.time
        self.descriptionLabel.text = cellModel.mainDescription
        self.tempLabel.text = cellModel.temperature
    }
}
